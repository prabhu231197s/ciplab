(function(){

    var express = require('express');
    var app = express();
    var cors = require('cors');
    var bodyParser = require('body-parser');
    var logger = require('morgan');
    var connection = require('./server/configs/connection');

    //----------------------------Middlewares------------------------------------//

    app.use(cors());
    app.use(bodyParser.json());
    app.use(logger("combined"));


    var routes = require('./server/index')(app);

    app.listen(3333,function(err){
        if(err) {
            console.log(err);
        }
        else {
            console.log('Running on 3333');
        }
    });

    module.exports = app;

})();