(function() {

    var express = require('express');
    var router = express.Router();
    var commonController = require('../controllers/commonController');

    router.get('/',function(req,res) {
        commonController.getHello(req,res);
    });

    router.post('/post',function(req,res) {
        commonController.postCheck(req,res);
    });

    module.exports = router;


})();