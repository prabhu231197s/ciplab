(function() {

    var responseHandler = {
        
        response : function(res,data) {
            var obj = {};
            obj.statusCode = 200;
            obj.message = "Success";
            obj.response = data;
            res.status(200).json(obj);
        },

        error : function(res,err) {
            var obj = {};
            obj.statusCode = err.statusCode!==undefined? err.statusCode : 500;
            obj.message = err.message!==undefined? err.message : "Something went wrong";
            obj.stack = err.stack!==undefined? err.stack : "Unknown error";
            obj.code = err.code!==undefined? err.code: "Unknown code";
            res.status(obj.statusCode).json(obj);
        }

    };

    module.exports = responseHandler;

})();