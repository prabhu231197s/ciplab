(function(){

    var commonService = require('../services/commonService');
    var responseHandler = require('../helpers/responseHandler');

    module.exports.getHello = function(req,res) {
        try {
            var str = 'Madaa';
            commonService.getHello(str,function(err,data){
                if(err) {
                    responseHandler.error(res,err);
                }
                else {
                    responseHandler.response(res,data);
                }
            });
        }
        catch(err) {
            responseHandler.error(res,err);
        }
    };

    module.exports.postCheck = function(req,res) {
        try {
            commonService.postCheck(req.body,function(err,data) {
                if(err) {
                    responseHandler.error(res,err);
                }
                else {
                    responseHandler.response(res,data);
                }
            });
        }
        catch(err) {
            responseHandler.error(res,err);
        }
    };

})();