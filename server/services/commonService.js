(function(){

    var commonDao = require('../dao/commonDao');

    module.exports.getHello = function(str,callback) {
        commonDao.getHello(str,callback);
    };

    module.exports.postCheck = function(body,callback) {
        commonDao.checkPost(body,callback);
    };

})();