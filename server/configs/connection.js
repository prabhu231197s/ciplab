(function(){

    var mysql = require('mysql');
    var config = require('../configs/config.json');
    config = config.dev;

    var connection = mysql.createConnection({
        host : config.host,
        port : config.port,
        user: config.user,
        password : config.password,
        db : config.db,
        mutipleStatements: config.mutipleStatements
    });

    connection.connect(function(err) {
        if(err) {
            console.log('Error connecting to thne DB:'+err);
        }
        else {
            console.log('App connected to the DB');
        }
    });
    

    module.exports = connection;

})();